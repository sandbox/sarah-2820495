(function($) {

  Drupal.behaviors.stakeholderReviewViewportDisplay = {
    attach: function (context, settings) {
      $('#stakeholder-review-handle a').click(function() {
        $('#stakeholder-review-viewport-container').slideToggle();
      });
    }
  };

})(jQuery);