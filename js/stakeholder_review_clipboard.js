(function($) {

  Drupal.behaviors.stakeholderReviewClipboard = {
    attach: function (context, settings) {
      var clipboard = new Clipboard('.btn');

      clipboard.on('success', function(e) {
        $('.stakeholder-review-input-group').once().after('<span class="stakeholder-review-copied">Copied!</span>');
      });

      clipboard.on('error', function(e) {
        $('.stakeholder-review-input-group').once().after('<span class="stakeholder-review-copied">Press Ctrl+C to copy</span>');
      });
    }
  };

})(jQuery);