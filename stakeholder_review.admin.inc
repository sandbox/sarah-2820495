<?php

/**
 * Configuration page callback.
 */
function stakeholder_review_settings_page() {
  $form = array();

  // Fieldsets.
  $form['stakeholder_role'] = array(
    '#type' => 'fieldset',
    '#title' => t('Stakeholder role'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['stakeholder_ui'] = array(
    '#type' => 'fieldset',
    '#title' => t('User interface'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Configuration for the Stakeholder role.
  // If we've already created a Stakeholder user, show a delete option instead.
  if ($uid = _stakeholder_review_get_stakeholder_uid()) {
    $form['stakeholder_role']['#description'] = '<p>' . t('You have already created a Stakeholder user. To create review URLs, click the <em>Stakeholder Review</em> tab on a node page.') . '</p>';
    $form['stakeholder_role']['#description'] .= '<p>' . t('Check the box below and submit the form to delete your Stakeholder user. Caution: This will break existing Stakeholder links.') .'</p>';
    $form['stakeholder_role']['delete_stakeholder'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete stakeholder user'),
    );
    $form['uid'] = array(
      '#type' => 'textfield',
      '#default_value' => $uid,
      '#access' => FALSE,
    );
  }
  // No roles have the right permission, so we can't create a Stakeholder user.
  // Prompt the user to set that up.
  else if ($roles = user_roles(TRUE, 'view any unpublished content')) {
    $form['stakeholder_role']['#collapsed'] = FALSE;
    $form['stakeholder_role']['#description'] = t('Select a role to give the Stakeholder user. You may need to ') . l('create a new role', 'admin/people/permissions/roles') . ' first.';
    $form['stakeholder_role']['role'] = array(
      '#type' => 'radios',
      '#title' => t('Role'),
      '#options' => $roles,
      '#description' => t('When you submit this form, a new user will be created with this role for use for Stakeholder Reviews.'),
    );
  }
  // Otherwise, allow the user to create the stakeholder user.
  else {
    drupal_set_message(t('No roles have the "view any unpublished content" permission. ') .
      l('Create a new role', 'admin/people/permissions/roles') . t(' for this, or ') .
      l('add that permission to an existing role', 'admin/people/permissions') . '. ' .
      t('View the ') . l('module help page', 'admin/help/stakeholder_review') . t(' for more info.'), 'warning');
    return;
  }

  // User interface options.
  $form['stakeholder_ui']['stakeholder_review_display'] = array(
    '#type' => 'radios',
    '#title' => t('Display preference'),
    '#options' => array(
      'tab' => 'Show Stakeholder Review in a menu tab.',
      'viewport' => 'Show Stakeholder Review in the corner of the viewport.'
    ),
    '#description' => t('Where should the Stakeholder Review URL appear?'),
    '#default_value' => variable_get('stakeholder_review_display', ''),
  );

  // @todo validation function to make sure the clipboardJS library exists
  // @todo add README about how to set this up
  $form['stakeholder_ui']['stakeholder_review_use_clipboardjs'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use clipboard.js'),
    '#description' => t('Include ') . l('clipboard.js', 'https://clipboardjs.com/') . t(' for an easier way for admins to copy review URLs.'),
    '#default_value' => variable_get('stakeholder_review_use_clipboardjs', '')
  );

  if (variable_get('stakeholder_review_help_text_set')) {
    $default_help = variable_get('stakeholder_review_help_text');
  }
  else {
    $default_help = _stakeholder_review_default_help_text();
  }
  $form['stakeholder_ui']['stakeholder_review_help_text'] = array(
    '#type' => 'textarea',
    '#title' => 'Help text',
    '#description' => t('This is shown underneath review URLs.'),
    '#default_value' => $default_help,
  );
  // Flag if the form has been submitted before.
  // This allows someone to save an empty box for help text,
  // if that's their thing.
  $form['stakeholder_ui']['stakeholder_review_help_text_set'] = array(
    '#type' => 'checkbox',
    '#access' => FALSE,
    '#default_value' => 1,
  );
  // @todo might be nice to have a revert to default button

  $form['#submit'][] = 'stakeholder_review_settings_form_submit';
  return system_settings_form($form);

}

/**
 * Default help text.
 * @return string
 */
function _stakeholder_review_default_help_text() {
  return t('The stakeholder will be able to browse through the site and see all unpublished content. They won\'t be able to edit content. They will see unpublished content in the same places that you see it (e.g. if you see unpublished content on a listing page, or shown in the main menu).');
}